/*
 * File: program.cpp
 * -----------------
 * This file is a stub implementation of the program.h interface
 * in which none of the methods do anything beyond returning a
 * value of the correct type.  Your job is to fill in the bodies
 * of each of these methods with an implementation that satisfies
 * the performance guarantees specified in the assignment.
 */

#include <string>
#include "program.h"
#include "statement.h"
using namespace std;

Program::Program() {
   _map.clear();
   // Replace this stub with your own code
}

Program::~Program() {
	_map.clear();
   // Replace this stub with your own code
}

void Program::clear() {
	_map.clear();
   // Replace this stub with your own code
}

void Program::addSourceLine(int lineNumber, string line) {
	_map[lineNumber] = SourceLine(line);
   // Replace this stub with your own code
}

void Program::removeSourceLine(int lineNumber) {
   // Replace this stub with your own code
	auto it = _map.find(lineNumber);
	if (it != _map.end())
		_map.erase(lineNumber);
}

string Program::getSourceLine(int lineNumber) {
   if (_map.find(lineNumber) != _map.end())
		return _map[lineNumber].line;
   else return "";   
}

void Program::setParsedStatement(int lineNumber, Statement *stmt) {
   // Replace this stub with your own code
	if (_map.find(lineNumber) != _map.end())
	{
		if (_map[lineNumber].statement != nullptr)
			delete _map[lineNumber].statement;
		_map[lineNumber].statement = stmt;
	}
	else error("SYNTAX ERROR");
}

Statement *Program::getParsedStatement(int lineNumber) {
	if (_map.find(lineNumber) != _map.end())
	{
		return _map[lineNumber].statement;
	}
	else error("SYNTAX ERROR");
}

int Program::getFirstLineNumber() {
	if (_map.empty())  return -1;
	else return _map.begin()->first;
}

int Program::getNextLineNumber(int lineNumber) {
	if (_map.find(lineNumber) == _map.end()) error("SYNTAX ERROR");
       else 
	    {
		   auto it = _map.find(lineNumber);
		   it++;
		   if (it == _map.end()) return -1;
		   return it->first;
        }
}
void Program::showlist() {
	for (auto it = _map.begin(); it != _map.end(); it++)
		cout << it->second.line << endl;
}
void Program::RUN(EvalState & state) {
	for (auto it = _map.begin(); it != _map.end(); ) 
	{
		Statement *statement = it->second.statement;
		int choice = statement->Type();
		if (choice == REM || choice == LET || choice == PRINT || choice == INPUT )
	    {
		  int tmp = statement->Execute(state);
		  it++;
		}
		if (choice == END) return;
		if (choice == GOTO)
		   {
			int jump = statement->Execute(state);
			it = _map.find(jump);
			if (it == _map.end()) error("LINE NUMBER ERROR");
		   }
        if (choice == IF)
		   {  
			int jump = statement->Execute(state);
			if (jump == -1) it++;
			else {
				it = _map.find(jump);
				if (it == _map.end()) error("LINE NUMBER ERROR");
			}
		   }
	}
}
